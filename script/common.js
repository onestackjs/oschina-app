/*
 * 向控制台输出日志
 * @param  data data  要输出的日志
 * @return string
 */
function log(data){
	if(typeof data == 'string'){
		console.log(data);
	}else{
		console.log(JSON.stringify(data));
	}
}

// 查看新闻列表中的内容详情
function open_detail( id, type, url ){

	if(arguments.length === 3){
		if( url.match(/oschina\.net\/p\//i) ){	// 软件
			var  identm = url.match(/\/p\/(.+)/);
			id = identm[1];
			type = 'software';
		}else if( url.match(/oschina\.net\/.+\/blog\//i) ){	//博客
			var  identm = url.match(/\/blog\/(\d+)/);
			id = identm[1];
			type = 'blog';
		}else if( url.match(/oschina\.net\/news\//i) ){	//新闻
			var  identm = url.match(/\/news\/(\d+)/);
			id = identm[1];
			type = 'news';
		}else if( url.match(/oschina\.net\/translate/i) ){	//翻译
			id = url;
			type = 'url';
		}else if( url.match(/oschina\.net\/question/i) ){	//帖子
			var  identm = url.match(/question\/\d+_(\d+)/);
			id = identm[1];
			type = 'post';
		}else{
			id = url;
			type = 'url';
		}
		
	}

	switch( type )
	{
	case 'news':
		api.openWin({
		    name: 'news_detail_win',
		    url: 'widget://html/news_detail_win.html',
		    pageParam: {id: id},
		});
	  break;
	case 'url':
		api.openWin({
		    name: 'url_win',
		    url: 'widget://html/url_win.html',
		    pageParam: {url: id},
		});
	  break;
	case 'software':
		api.openWin({
		    name: 'software_detail_win',
		    url: 'widget://html/software_detail_win.html',
		    pageParam: {ident: id},
		});
	  break;
	case 'post':
		api.openWin({
		    name: 'post_detail_win',
		    url: 'widget://html/post_detail_win.html',
		    pageParam: {id: id},
		});
	  break;
	case 'blog':
		api.openWin({
		    name: 'blog_detail_win',
		    url: 'widget://html/blog_detail_win.html',
		    pageParam: {id: id},
		});
	  break;
	case 'tweet':
		openTweet( id );
	  break;
	default:
		api.toast({
		    msg: '未知类型',
		    duration:2000,
		    location: 'middle'
		});
	}
	
}

// 判断新闻列表中新闻的类型
function judgeType( obj ){

	var url = obj.url;
	var newstype = parseInt(obj.newstype.type);
	
	// 由于 trans 模块在 iOS 上转换出来的空节点是object，而android上得到了是空
	// 所以这里进行一下特殊处理
	if(typeof(url) == 'object'){
		url = '';
	}
	
	if( newstype == 0 && url == ''){		// 普通新闻
		this.id 	= obj.id;
		this.type 	= 'news';
	}else if( newstype == 0 && url.length > 0 ){		// 链接新闻
		this.id 	= url;
		this.type 	= 'url';
	}else if( newstype == 1){		// 软件推荐
		this.id 	= obj.newstype.attachment;
		this.type 	= 'software';
	}else if( newstype == 2){		// 讨论区帖子/活动
		this.id 	= obj.newstype.attachment;
		this.type 	= 'post';
	}else if( newstype == 3){		// 博客
		this.id 	= obj.newstype.attachment;
		this.type 	= 'blog';
	}else{
		this.id 	= obj.id;
		this.type 	= 'other';
	}
	
	return this;
}

// 收藏
function favorite_add(objid, type, uid){
	objid 	= intval(objid);
	type 	= intval(type);
	uid 	= intval(uid);
	if( uid<1 ){
		api.toast({
		    msg: '请先登录',
		    duration:2000,
		    location: 'middle'
		});
		return false;
	}
	
	if(objid<1 && type<1){
		api.toast({
		    msg: '参数错误',
		    duration:2000,
		    location: 'middle'
		});
		return false;
	}
	
	var url = OpenAPI.favorite_add;
	api.ajax({
	    url: url,
	    method: 'post',
	    timeout: 30,
	    dataType: 'text',
	    returnAll:false,
	    data:{
	        values: {
	        	objid: objid,
	        	type: type,
	        	uid: uid,
	        },
	    }
	},function(ret,err){
	    if (ret) {
			var trans = api.require('trans');
			trans.parse({
			    data:ret
			},function(ret,err){
			    if(ret) {
			    	set_favorite_ico( 1 , objid);
					api.toast({
					    msg: '添加收藏成功',
					    duration:2000,
					    location: 'middle'
					});
			    }else{
			        log(err.msg);
			    }
			});
	    }else {
	        log('错误码：'+err.code+'；错误信息：'+err.msg+'网络状态码：'+err.statusCode);
	    	showAjaxErrMsg(err.code);
	    }
	});
}

// 取消收藏
function favorite_delete(objid, type, uid){
	objid 	= intval(objid);
	type 	= intval(type);
	uid 	= intval(uid);
	if( uid<1 ){
		api.toast({
		    msg: '请先登录',
		    duration:2000,
		    location: 'middle'
		});
		return false;
	}
	
	if(objid<1 && type<1){
		api.toast({
		    msg: '参数错误',
		    duration:2000,
		    location: 'middle'
		});
		return false;
	}
	
	var url = OpenAPI.favorite_delete;
	api.ajax({
	    url: url,
	    method: 'post',
	    timeout: 30,
	    dataType: 'text',
	    returnAll:false,
	    data:{
	        values: {
	        	objid: objid,
	        	type: type,
	        	uid: uid,
	        },
	    }
	},function(ret,err){
	    if (ret) {
			var trans = api.require('trans');
			trans.parse({
			    data:ret
			},function(ret,err){
			    if(ret) {
			    	set_favorite_ico( 0 , objid);
					api.toast({
					    msg: '取消收藏成功',
					    duration:2000,
					    location: 'middle'
					});
			    }else{
			        log(err.msg);
			    }
			});
	    }else {
	        log('错误码：'+err.code+'；错误信息：'+err.msg+'网络状态码：'+err.statusCode);
	    	showAjaxErrMsg(err.code);
	    }
	});
}

/*
 * 设置收藏图标
 * @param  type  1: 已收藏   0: 未收藏
 */
function set_favorite_ico( type, objid ){
	var favor = $api.byId('favor');
	var iconfavor = $api.byId('icon-favor');

	if( parseInt(type)==1 ){
		$api.attr(favor, 'onclick', 'favorite(0, '+objid+')');
		if($api.hasCls(iconfavor, 'aui-icon-favor')){
			$api.removeCls(iconfavor, 'aui-icon-favor');
		}
		$api.addCls(iconfavor, 'aui-icon-favorfill');
		$api.addCls(iconfavor, 'aui-text-warning');
	}else{
		$api.attr(favor, 'onclick', 'favorite(1, '+objid+')');
		if($api.hasCls(iconfavor, 'aui-icon-favorfill')){
			$api.removeCls(iconfavor, 'aui-icon-favorfill');
		}
		if($api.hasCls(iconfavor, 'aui-text-warning')){
			$api.removeCls(iconfavor, 'aui-text-warning');
		}
		$api.addCls(iconfavor, 'aui-icon-favor');
	}
	
}

// 通过新闻\帖子frm设置win的评论按钮
function set_comment_button( id, catalog, commentCount ){
	if( parseInt(commentCount)>0 ){
		$('#commentCount').text(commentCount);
		$('#commentCount').show();
	}
	var comment = $api.byId('comment');
	$api.attr(comment, 'onclick', 'openCommentList( '+id+', '+catalog+' )');
	setTimeout("api.parseTapmode()", 50);
}

// 打开新闻\帖子评论列表页
function openCommentList( id, catalog ){
	api.openWin({
	    name: 'comment_list_win',
	    url: 'widget://html/comment_list_win.html',
	    pageParam: {id: id, catalog: catalog},
	});
}

// 通过博客frm设置win的评论按钮
function set_blogcomment_button( id, commentCount ){
	if( parseInt(commentCount)>0 ){
		$('#commentCount').text(commentCount);
		$('#commentCount').show();
	}
	var comment = $api.byId('comment');
	$api.attr(comment, 'onclick', 'openBlogCommentList( '+id+' )');
	setTimeout("api.parseTapmode()", 50);
}

// 打开博客评论列表页
function openBlogCommentList( id ){
	api.openWin({
	    name: 'comment_list_win',
	    url: 'widget://html/blogcomment_list_win.html',
	    pageParam: {id: id, },
	});
}

// 通过软件frm设置win的评论按钮
function set_softwarecomment_button( id, commentCount ){
	if( parseInt(commentCount)>0 ){
		$('#commentCount').text(commentCount);
		$('#commentCount').show();
	}
	var comment = $api.byId('comment');
	$api.attr(comment, 'onclick', 'openSoftwareCommentList( '+id+' )');
	setTimeout("api.parseTapmode()", 50);
}

// 打开软件评论列表页
function openSoftwareCommentList( id ){
	api.openWin({
	    name: 'softwarecomment_list_win',
	    url: 'widget://html/softwarecomment_list_win.html',
	    pageParam: {project: id, },
	});
}

// 获取APP客户端类型名称
function getAppClientName( appclient ){
	if(arguments.length === 0){	// 不传参数，则获取当前客户端类型名称
		var sysType = api.systemType;
		var name = '';
        if(sysType == 'ios'){
            name = 'iPhone';
        }else if(sysType == 'android'){
            name = 'Android';
        }else{
        	name = 'Other';
        }
        return name;
	}
	
	switch( parseInt(appclient) )
	{
	case 0:
	  appclient = 'PC';
	  break;
	case 1:
	  appclient = 'PC';
	  break;
	case 3:
	  appclient = 'Android';
	  break;
	case 4:
	  appclient = 'iPhone';
	  break;
	default:
	  appclient = 'Other';
	}
	return appclient;
}

// 打开动弹详情页面
function openTweet( id ){
	api.openWin({
	    name: 'tweet_detail_win',
	    url: 'widget://html/tweet_detail_win.html',
	    pageParam: {id: id, },
	});
}

// 打开他人的用户中心
function openUserInfo( hisuid ){
	api.openWin({
	    name: 'user_information_win',
	    url: 'widget://html/user_information_win.html',
	    pageParam: {hisuid: hisuid},
	});
}

// 隐藏loading
function hideLoading(){
    $api.remove($api.dom('.aui-loading'));
}

// 显示loading
function showLoading(){
    var loading = $api.dom(".aui-loading");
    if(!loading){
        $api.append($api.dom('body'),'<div class="aui-loading"><div class="aui-loading-1"></div><div class="aui-loading-2"></div></div>');
    }
}

// 判断用户是否登录，但这种判断方式不太准确
// 由于使用Cookie 跟踪用户，但APICLOUD对Cookie支持不好，所以我也没啥好办法
function isLogin(){
	var user_uid = parseInt($api.getStorage('user_uid'));
	if(user_uid){
		return true;
	}else{
		return false;
	}
}

// 打开登录页
function openLogin(){
	if(isLogin()){
		return false;
	}
	api.openWin({
	    name: 'login_win',
	    url: 'widget://html/login_win.html',
	});
}

/*
 * 清除用户信息
 * @param  bool refreshUserPage  要输出的日志
 * @return string
 */
function clearUserInfo( refreshUserPage ){

	$api.setStorage('user_uid', 0);
	$api.rmStorage('user_favoritecount');
	$api.rmStorage('user_followers');
	$api.rmStorage('user_location');
	$api.rmStorage('user_fans');
	$api.rmStorage('user_name');
	$api.rmStorage('user_score');
	$api.rmStorage('user_gender');
	$api.rmStorage('user_portrait');
	$api.rmStorage('user_jointime');
	$api.rmStorage('user_devplatform');
	$api.rmStorage('user_expertise');
	$api.rmStorage('notice_atmeCount');		// 新@我个数
	$api.rmStorage('notice_msgCount');		// 新私信个数
	$api.rmStorage('notice_reviewCount');	// 新评论个数
	$api.rmStorage('notice_newFansCount');// 新评论个数
	$api.rmStorage('notice_newLikeCount');// 
	$api.rmStorage('notice_count');// 总的新通知个数
	
	$api.rmStorage('rong_Token');// 融云Token
	
	if( refreshUserPage ){
		api.execScript({
		    name: 'root',
		    frameName: 'footer_tab_3',
		    script: 'api.refreshHeaderLoading();'
		});
	}

}

// 统一设置所有页面的通知图标
function setAllNoticeIco( notice_count ){
	// 设置应用图标右上角数字，支持所有iOS手机，以及部分Android手机，如小米和三星的某些型号，不支持的设备，表现结果为调用该接口无任何效果
	api.setAppIconBadge({
	    badge: notice_count
	});
	$api.setStorage('notice_count', notice_count);

	// 设置 root 的收藏图标状态
	api.execScript({
	    name: 'root',
	    script: 'setNoticeIco( '+notice_count+' )'
	});

	// 设置 footer_tab_3 的收藏图标状态
	api.execScript({
	    name: 'root',
	    frameName: 'footer_tab_3',
	    script: 'setNoticeIco( '+notice_count+' )'
	});
}

//友好的时间返回函数
function friendly_time(time_stamp)
{
	time_stamp = strtotime(time_stamp);	// 把日期时间解析为 Unix 时间戳 
    var now_d = new Date();
    var now_time = now_d.getTime() / 1000; //获取当前时间的秒数
    var f_d = new Date();
    f_d.setTime(time_stamp * 1000);
    var f_time = f_d.toLocaleDateString();

    var ct = now_time - time_stamp;
    var day = 0;
    if (ct < 0)
    {
        f_time = "【预约】" + f_d.toLocaleString();
    }
    else if (ct < 60)
    {
        f_time = Math.floor(ct) + '秒前';
    }
    else if (ct < 3600)
    {
        f_time = Math.floor(ct / 60) + '分钟前';
    }
    else if (ct < 86400)//一天
    {
        f_time = Math.floor(ct / 3600) + '小时前';
    }
    else if (ct < 604800)//7天
    {
        day = Math.floor(ct / 86400);
        if (day < 2)
            f_time = '昨天';
        else
            f_time = day + '天前';
    }
    else
    {
        day = Math.floor(ct / 86400);
        f_time = day + '天前';
    }
    return f_time;
}

// 浏览图片
function imageBrowser( imageUrl ){
		if( imageUrl.length<1 ){
			return false;
		}
		
		var obj = api.require('imageBrowser');
		obj.openImages({
		    imageUrls: [imageUrl],
		    showList:false,
		    activeIndex:0
		});
}

// 动弹列表点赞
function tweetListMakeAsLove( tweetid, authorid ){
	var uid = parseInt($api.getStorage('user_uid'));
	if( uid<1 ){
		api.toast({
		    msg: '请先登录',
		    duration:2000,
		    location: 'middle'
		});
		return false;
	}
	
	var isLike = $api.byId('isLike_'+tweetid);
	if( parseInt($api.attr(isLike, 'isLike'))==1 ){		// 已经赞了的不可再赞
		return false;
	}

	var url = OpenAPI.tweet_like;
	api.ajax({
	    url: url,
	    method: 'post',
	    timeout: 30,
	    dataType: 'text',
	    returnAll:false,
	    data:{
	        values: {
	        	ownerOfTweet: authorid,
	        	tweetid: tweetid,
	        	uid: uid,
	        },
	    }
	},function(ret,err){
	    if (ret) {
			var trans = api.require('trans');
			trans.parse({
			    data:ret
			},function(ret,err){
			    if(ret && parseInt(ret.oschina.result.errorCode)==1 ) {
			    	$api.attr(isLike,'isLike', 1);
			    	$api.removeCls(isLike, 'aui-icon-appreciate');
			    	$api.addCls(isLike, 'aui-icon-appreciatefill');
			    	$api.addCls(isLike, 'aui-text-primary');

			    	var userList = $api.byId('userList_'+tweetid);
			    	if( userList ){		// 用户列表已存在
			    		var userList = $api.byId('userList_'+tweetid);
			    		var newUserList = $api.getStorage('user_name') + '、' + $api.text(userList);
			    		$api.text(userList, newUserList);
			    		
			    		var likeCount = $api.byId('likeCount_'+tweetid);
			    		var newLikeCount = parseInt( $api.text(likeCount) ) + 1;
			    		$api.text(likeCount, newLikeCount);
			    		
			    	}else{	// 用户列表还不存在
			    		var likeList = $api.byId('likeList_'+tweetid);
						var likeHtml = '<font class="aui-text-primary" id="userList_'+tweetid+'">'+ $api.getStorage('user_name') +'</font> ' + '<font id="likeCount_'+tweetid+'">1</font>人觉得很赞'
			    		$api.html(likeList, likeHtml);
			    	}
			    	
			    }else{
			        log(err.msg);
			    }
			});
	    }else {
	        log('错误码：'+err.code+'；错误信息：'+err.msg+'网络状态码：'+err.statusCode);
	    	//showAjaxErrMsg(err.code);
	    }
	});
}

// ajax错误信息显示
function showAjaxErrMsg(code){
	var msg = '';
	switch( parseInt(code) )
	{
	case 0:
	  	msg = '无法连接网络，请检查网络配置';
	  break;
	case 1:
	  	msg = '连接超时，请稍后再试';
	  break;
	case 2:
	  	msg = '授权错误';
	  break;
	case 3:
	  	msg = '数据类型错误';
	  break;
	}
	api.toast({
	    msg: msg,
	    duration:2000,
	    location: 'middle'
	});
}

// 根据用户设置，改变字体大小
function changeContentFontSize(){
	var contentFontSize = $api.getStorage('contentFontSize');
	$api.addCls($api.byId('content'), 'font_size_'+contentFontSize);
}

/*
 * 页面向上滚到顶部
 */
function scroll() {
	api.pageUp(
		{top: true},
	    function(ret){}
	);
}

// 清除消息
function notice_clear( type ){
	var notice_atmeCount 	=  parseInt( $api.getStorage('notice_atmeCount') );
	var notice_reviewCount 	=  parseInt( $api.getStorage('notice_reviewCount') );
	var notice_msgCount 	=  parseInt( $api.getStorage('notice_msgCount') );
	var notice_newFansCount =  parseInt( $api.getStorage('notice_newFansCount') );
	var notice_newLikeCount =  parseInt( $api.getStorage('notice_newLikeCount') );

	if( type==1 && notice_atmeCount<1 ){
		return false;
	}else if( type==3 && notice_reviewCount<1 ){
		return false;
	}else if( type==2 && notice_msgCount<1 ){
		return false;
	}else if( type==4 && notice_newFansCount<1 ){
		return false;
	}
	
	var url = OpenAPI.notice_clear;
	api.ajax({
	    url: url,
	    method: 'post',
	    timeout: 30,
	    dataType: 'text',
	    returnAll:false,
	    data:{
	        values: {
	        type: type,
	        uid: uid,
	        },
	    }
	},function(ret,err){
	    if (ret) {
			var trans = api.require('trans');
			trans.parse({
			    data:ret
			},function(ret,err){
			    if(ret) {
					if(typeof(ret.oschina.result.errorCode) != 'undefined' && parseInt(ret.oschina.result.errorCode)==1){
						if( type==1 ){
							$api.setStorage('notice_atmeCount', 0);
						}else if( type==3 ){
							$api.setStorage('notice_reviewCount', 0);
						}else if( type==2 ){
							$api.setStorage('notice_msgCount', 0);
						}else if( type==4 ){
							$api.setStorage('notice_newFansCount', 0);
						}
						
						// 设置选项卡上的新通知数
						api.execScript({
						    name: 'notice_win',
						    script: 'setTabCount();'
						});
						
						// 设置所有页面的通知图标
						var notice_count = parseInt($api.getStorage('notice_atmeCount')) + parseInt($api.getStorage('notice_reviewCount')) + parseInt($api.getStorage('notice_msgCount')) + parseInt($api.getStorage('notice_newFansCount')) + parseInt($api.getStorage('notice_newLikeCount'));
						setAllNoticeIco( notice_count );
					}else if( typeof(ret.oschina.result.errorMessage) != 'undefined' ){
						log(ret.oschina.result.errorMessage);
					}
			    }else{
			      	log(err.msg);
			    }
			});
	    }else {
	        log('错误码：'+err.code+'；错误信息：'+err.msg+'网络状态码：'+err.statusCode);
	    	showAjaxErrMsg(err.code);
	    }
	});
}

//============================
//			phpjs
//============================

// 在数组中搜索给定的值
function in_array(needle, haystack, argStrict) {
  //  discuss at: http://phpjs.org/functions/in_array/
  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // improved by: vlado houba
  // improved by: Jonas Sciangula Street (Joni2Back)
  //    input by: Billy
  // bugfixed by: Brett Zamir (http://brett-zamir.me)
  //   example 1: in_array('van', ['Kevin', 'van', 'Zonneveld']);
  //   returns 1: true
  //   example 2: in_array('vlado', {0: 'Kevin', vlado: 'van', 1: 'Zonneveld'});
  //   returns 2: false
  //   example 3: in_array(1, ['1', '2', '3']);
  //   example 3: in_array(1, ['1', '2', '3'], false);
  //   returns 3: true
  //   returns 3: true
  //   example 4: in_array(1, ['1', '2', '3'], true);
  //   returns 4: false

  var key = '',
    strict = !! argStrict;

  //we prevent the double check (strict && arr[key] === ndl) || (!strict && arr[key] == ndl)
  //in just one for, in order to improve the performance 
  //deciding wich type of comparation will do before walk array
  if (strict) {
    for (key in haystack) {
      if (haystack[key] === needle) {
        return true;
      }
    }
  } else {
    for (key in haystack) {
      if (haystack[key] == needle) {
        return true;
      }
    }
  }

  return false;
}

// 剥去字符串中的 HTML 标签
function strip_tags(input, allowed) {
  //  discuss at: http://phpjs.org/functions/strip_tags/
  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // improved by: Luke Godfrey
  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  //    input by: Pul
  //    input by: Alex
  //    input by: Marc Palau
  //    input by: Brett Zamir (http://brett-zamir.me)
  //    input by: Bobby Drake
  //    input by: Evertjan Garretsen
  // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // bugfixed by: Onno Marsman
  // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // bugfixed by: Eric Nagel
  // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // bugfixed by: Tomasz Wesolowski
  //  revised by: Rafał Kukawski (http://blog.kukawski.pl/)
  //   example 1: strip_tags('<p>Kevin</p> <br /><b>van</b> <i>Zonneveld</i>', '<i><b>');
  //   returns 1: 'Kevin <b>van</b> <i>Zonneveld</i>'
  //   example 2: strip_tags('<p>Kevin <img src="someimage.png" onmouseover="someFunction()">van <i>Zonneveld</i></p>', '<p>');
  //   returns 2: '<p>Kevin van Zonneveld</p>'
  //   example 3: strip_tags("<a href='http://kevin.vanzonneveld.net'>Kevin van Zonneveld</a>", "<a>");
  //   returns 3: "<a href='http://kevin.vanzonneveld.net'>Kevin van Zonneveld</a>"
  //   example 4: strip_tags('1 < 5 5 > 1');
  //   returns 4: '1 < 5 5 > 1'
  //   example 5: strip_tags('1 <br/> 1');
  //   returns 5: '1  1'
  //   example 6: strip_tags('1 <br/> 1', '<br>');
  //   returns 6: '1 <br/> 1'
  //   example 7: strip_tags('1 <br/> 1', '<br><br/>');
  //   returns 7: '1 <br/> 1'

  allowed = (((allowed || '') + '')
      .toLowerCase()
      .match(/<[a-z][a-z0-9]*>/g) || [])
    .join(''); // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
  var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
    commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
  return input.replace(commentsAndPhpTags, '')
    .replace(tags, function($0, $1) {
      return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
    });
}

// 移除数组中的重复的值,并返回结果数组
function array_unique(inputArr) {
  //  discuss at: http://phpjs.org/functions/array_unique/
  // original by: Carlos R. L. Rodrigues (http://www.jsfromhell.com)
  //    input by: duncan
  //    input by: Brett Zamir (http://brett-zamir.me)
  // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // bugfixed by: Nate
  // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // bugfixed by: Brett Zamir (http://brett-zamir.me)
  // improved by: Michael Grier
  //        note: The second argument, sort_flags is not implemented;
  //        note: also should be sorted (asort?) first according to docs
  //   example 1: array_unique(['Kevin','Kevin','van','Zonneveld','Kevin']);
  //   returns 1: {0: 'Kevin', 2: 'van', 3: 'Zonneveld'}
  //   example 2: array_unique({'a': 'green', 0: 'red', 'b': 'green', 1: 'blue', 2: 'red'});
  //   returns 2: {a: 'green', 0: 'red', 1: 'blue'}

  var key = '',
    tmp_arr2 = {},
    val = '';

  var __array_search = function(needle, haystack) {
    var fkey = '';
    for (fkey in haystack) {
      if (haystack.hasOwnProperty(fkey)) {
        if ((haystack[fkey] + '') === (needle + '')) {
          return fkey;
        }
      }
    }
    return false;
  };

  for (key in inputArr) {
    if (inputArr.hasOwnProperty(key)) {
      val = inputArr[key];
      if (false === __array_search(val, tmp_arr2)) {
        tmp_arr2[key] = val;
      }
    }
  }

  return tmp_arr2;
}

// 获取变量的整数值
function intval(mixed_var, base) {
  //  discuss at: http://phpjs.org/functions/intval/
  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // improved by: stensi
  // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // bugfixed by: Brett Zamir (http://brett-zamir.me)
  // bugfixed by: Rafał Kukawski (http://kukawski.pl)
  //    input by: Matteo
  //   example 1: intval('Kevin van Zonneveld');
  //   returns 1: 0
  //   example 2: intval(4.2);
  //   returns 2: 4
  //   example 3: intval(42, 8);
  //   returns 3: 42
  //   example 4: intval('09');
  //   returns 4: 9
  //   example 5: intval('1e', 16);
  //   returns 5: 30

  var tmp;

  var type = typeof mixed_var;

  if (type === 'boolean') {
    return +mixed_var;
  } else if (type === 'string') {
    tmp = parseInt(mixed_var, base || 10);
    return (isNaN(tmp) || !isFinite(tmp)) ? 0 : tmp;
  } else if (type === 'number' && isFinite(mixed_var)) {
    return mixed_var | 0;
  } else {
    return 0;
  }
}

// 检测变量是否为数字或数字字符串
function is_numeric(mixed_var) {
  //  discuss at: http://phpjs.org/functions/is_numeric/
  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // improved by: David
  // improved by: taith
  // bugfixed by: Tim de Koning
  // bugfixed by: WebDevHobo (http://webdevhobo.blogspot.com/)
  // bugfixed by: Brett Zamir (http://brett-zamir.me)
  // bugfixed by: Denis Chenu (http://shnoulle.net)
  //   example 1: is_numeric(186.31);
  //   returns 1: true
  //   example 2: is_numeric('Kevin van Zonneveld');
  //   returns 2: false
  //   example 3: is_numeric(' +186.31e2');
  //   returns 3: true
  //   example 4: is_numeric('');
  //   returns 4: false
  //   example 5: is_numeric([]);
  //   returns 5: false
  //   example 6: is_numeric('1 ');
  //   returns 6: false

  var whitespace =
    " \n\r\t\f\x0b\xa0\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u200b\u2028\u2029\u3000";
  return (typeof mixed_var === 'number' || (typeof mixed_var === 'string' && whitespace.indexOf(mixed_var.slice(-1)) ===
    -
    1)) && mixed_var !== '' && !isNaN(mixed_var);
}

// 返回当前的 Unix 时间戳
function time() {
  //  discuss at: http://phpjs.org/functions/time/
  // original by: GeekFG (http://geekfg.blogspot.com)
  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // improved by: metjay
  // improved by: HKM
  //   example 1: timeStamp = time();
  //   example 1: timeStamp > 1000000000 && timeStamp < 2000000000
  //   returns 1: true

  return Math.floor(new Date()
    .getTime() / 1000);
}

//格式化一个本地时间／日期 
function date(format, timestamp) {
  //  discuss at: http://phpjs.org/functions/date/
  // original by: Carlos R. L. Rodrigues (http://www.jsfromhell.com)
  // original by: gettimeofday
  //    parts by: Peter-Paul Koch (http://www.quirksmode.org/js/beat.html)
  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // improved by: MeEtc (http://yass.meetcweb.com)
  // improved by: Brad Touesnard
  // improved by: Tim Wiel
  // improved by: Bryan Elliott
  // improved by: David Randall
  // improved by: Theriault
  // improved by: Theriault
  // improved by: Brett Zamir (http://brett-zamir.me)
  // improved by: Theriault
  // improved by: Thomas Beaucourt (http://www.webapp.fr)
  // improved by: JT
  // improved by: Theriault
  // improved by: Rafał Kukawski (http://blog.kukawski.pl)
  // improved by: Theriault
  //    input by: Brett Zamir (http://brett-zamir.me)
  //    input by: majak
  //    input by: Alex
  //    input by: Martin
  //    input by: Alex Wilson
  //    input by: Haravikk
  // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // bugfixed by: majak
  // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // bugfixed by: Brett Zamir (http://brett-zamir.me)
  // bugfixed by: omid (http://phpjs.org/functions/380:380#comment_137122)
  // bugfixed by: Chris (http://www.devotis.nl/)
  //        note: Uses global: php_js to store the default timezone
  //        note: Although the function potentially allows timezone info (see notes), it currently does not set
  //        note: per a timezone specified by date_default_timezone_set(). Implementers might use
  //        note: this.php_js.currentTimezoneOffset and this.php_js.currentTimezoneDST set by that function
  //        note: in order to adjust the dates in this function (or our other date functions!) accordingly
  //   example 1: date('H:m:s \\m \\i\\s \\m\\o\\n\\t\\h', 1062402400);
  //   returns 1: '09:09:40 m is month'
  //   example 2: date('F j, Y, g:i a', 1062462400);
  //   returns 2: 'September 2, 2003, 2:26 am'
  //   example 3: date('Y W o', 1062462400);
  //   returns 3: '2003 36 2003'
  //   example 4: x = date('Y m d', (new Date()).getTime()/1000);
  //   example 4: (x+'').length == 10 // 2009 01 09
  //   returns 4: true
  //   example 5: date('W', 1104534000);
  //   returns 5: '53'
  //   example 6: date('B t', 1104534000);
  //   returns 6: '999 31'
  //   example 7: date('W U', 1293750000.82); // 2010-12-31
  //   returns 7: '52 1293750000'
  //   example 8: date('W', 1293836400); // 2011-01-01
  //   returns 8: '52'
  //   example 9: date('W Y-m-d', 1293974054); // 2011-01-02
  //   returns 9: '52 2011-01-02'

  var that = this;
  var jsdate, f;
  // Keep this here (works, but for code commented-out below for file size reasons)
  // var tal= [];
  var txt_words = [
    'Sun', 'Mon', 'Tues', 'Wednes', 'Thurs', 'Fri', 'Satur',
    'January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December'
  ];
  // trailing backslash -> (dropped)
  // a backslash followed by any character (including backslash) -> the character
  // empty string -> empty string
  var formatChr = /\\?(.?)/gi;
  var formatChrCb = function(t, s) {
    return f[t] ? f[t]() : s;
  };
  var _pad = function(n, c) {
    n = String(n);
    while (n.length < c) {
      n = '0' + n;
    }
    return n;
  };
  f = {
    // Day
    d : function() {
      // Day of month w/leading 0; 01..31
      return _pad(f.j(), 2);
    },
    D : function() {
      // Shorthand day name; Mon...Sun
      return f.l()
        .slice(0, 3);
    },
    j : function() {
      // Day of month; 1..31
      return jsdate.getDate();
    },
    l : function() {
      // Full day name; Monday...Sunday
      return txt_words[f.w()] + 'day';
    },
    N : function() {
      // ISO-8601 day of week; 1[Mon]..7[Sun]
      return f.w() || 7;
    },
    S : function() {
      // Ordinal suffix for day of month; st, nd, rd, th
      var j = f.j();
      var i = j % 10;
      if (i <= 3 && parseInt((j % 100) / 10, 10) == 1) {
        i = 0;
      }
      return ['st', 'nd', 'rd'][i - 1] || 'th';
    },
    w : function() {
      // Day of week; 0[Sun]..6[Sat]
      return jsdate.getDay();
    },
    z : function() {
      // Day of year; 0..365
      var a = new Date(f.Y(), f.n() - 1, f.j());
      var b = new Date(f.Y(), 0, 1);
      return Math.round((a - b) / 864e5);
    },

    // Week
    W : function() {
      // ISO-8601 week number
      var a = new Date(f.Y(), f.n() - 1, f.j() - f.N() + 3);
      var b = new Date(a.getFullYear(), 0, 4);
      return _pad(1 + Math.round((a - b) / 864e5 / 7), 2);
    },

    // Month
    F : function() {
      // Full month name; January...December
      return txt_words[6 + f.n()];
    },
    m : function() {
      // Month w/leading 0; 01...12
      return _pad(f.n(), 2);
    },
    M : function() {
      // Shorthand month name; Jan...Dec
      return f.F()
        .slice(0, 3);
    },
    n : function() {
      // Month; 1...12
      return jsdate.getMonth() + 1;
    },
    t : function() {
      // Days in month; 28...31
      return (new Date(f.Y(), f.n(), 0))
        .getDate();
    },

    // Year
    L : function() {
      // Is leap year?; 0 or 1
      var j = f.Y();
      return j % 4 === 0 & j % 100 !== 0 | j % 400 === 0;
    },
    o : function() {
      // ISO-8601 year
      var n = f.n();
      var W = f.W();
      var Y = f.Y();
      return Y + (n === 12 && W < 9 ? 1 : n === 1 && W > 9 ? -1 : 0);
    },
    Y : function() {
      // Full year; e.g. 1980...2010
      return jsdate.getFullYear();
    },
    y : function() {
      // Last two digits of year; 00...99
      return f.Y()
        .toString()
        .slice(-2);
    },

    // Time
    a : function() {
      // am or pm
      return jsdate.getHours() > 11 ? 'pm' : 'am';
    },
    A : function() {
      // AM or PM
      return f.a()
        .toUpperCase();
    },
    B : function() {
      // Swatch Internet time; 000..999
      var H = jsdate.getUTCHours() * 36e2;
      // Hours
      var i = jsdate.getUTCMinutes() * 60;
      // Minutes
      // Seconds
      var s = jsdate.getUTCSeconds();
      return _pad(Math.floor((H + i + s + 36e2) / 86.4) % 1e3, 3);
    },
    g : function() {
      // 12-Hours; 1..12
      return f.G() % 12 || 12;
    },
    G : function() {
      // 24-Hours; 0..23
      return jsdate.getHours();
    },
    h : function() {
      // 12-Hours w/leading 0; 01..12
      return _pad(f.g(), 2);
    },
    H : function() {
      // 24-Hours w/leading 0; 00..23
      return _pad(f.G(), 2);
    },
    i : function() {
      // Minutes w/leading 0; 00..59
      return _pad(jsdate.getMinutes(), 2);
    },
    s : function() {
      // Seconds w/leading 0; 00..59
      return _pad(jsdate.getSeconds(), 2);
    },
    u : function() {
      // Microseconds; 000000-999000
      return _pad(jsdate.getMilliseconds() * 1000, 6);
    },

    // Timezone
    e : function() {
      // Timezone identifier; e.g. Atlantic/Azores, ...
      // The following works, but requires inclusion of the very large
      // timezone_abbreviations_list() function.
      /*              return that.date_default_timezone_get();
       */
      throw 'Not supported (see source code of date() for timezone on how to add support)';
    },
    I : function() {
      // DST observed?; 0 or 1
      // Compares Jan 1 minus Jan 1 UTC to Jul 1 minus Jul 1 UTC.
      // If they are not equal, then DST is observed.
      var a = new Date(f.Y(), 0);
      // Jan 1
      var c = Date.UTC(f.Y(), 0);
      // Jan 1 UTC
      var b = new Date(f.Y(), 6);
      // Jul 1
      // Jul 1 UTC
      var d = Date.UTC(f.Y(), 6);
      return ((a - c) !== (b - d)) ? 1 : 0;
    },
    O : function() {
      // Difference to GMT in hour format; e.g. +0200
      var tzo = jsdate.getTimezoneOffset();
      var a = Math.abs(tzo);
      return (tzo > 0 ? '-' : '+') + _pad(Math.floor(a / 60) * 100 + a % 60, 4);
    },
    P : function() {
      // Difference to GMT w/colon; e.g. +02:00
      var O = f.O();
      return (O.substr(0, 3) + ':' + O.substr(3, 2));
    },
    T : function() {
      // Timezone abbreviation; e.g. EST, MDT, ...
      // The following works, but requires inclusion of the very
      // large timezone_abbreviations_list() function.
      /*              var abbr, i, os, _default;
      if (!tal.length) {
        tal = that.timezone_abbreviations_list();
      }
      if (that.php_js && that.php_js.default_timezone) {
        _default = that.php_js.default_timezone;
        for (abbr in tal) {
          for (i = 0; i < tal[abbr].length; i++) {
            if (tal[abbr][i].timezone_id === _default) {
              return abbr.toUpperCase();
            }
          }
        }
      }
      for (abbr in tal) {
        for (i = 0; i < tal[abbr].length; i++) {
          os = -jsdate.getTimezoneOffset() * 60;
          if (tal[abbr][i].offset === os) {
            return abbr.toUpperCase();
          }
        }
      }
      */
      return 'UTC';
    },
    Z : function() {
      // Timezone offset in seconds (-43200...50400)
      return -jsdate.getTimezoneOffset() * 60;
    },

    // Full Date/Time
    c : function() {
      // ISO-8601 date.
      return 'Y-m-d\\TH:i:sP'.replace(formatChr, formatChrCb);
    },
    r : function() {
      // RFC 2822
      return 'D, d M Y H:i:s O'.replace(formatChr, formatChrCb);
    },
    U : function() {
      // Seconds since UNIX epoch
      return jsdate / 1000 | 0;
    }
  };
  this.date = function(format, timestamp) {
    that = this;
    jsdate = (timestamp === undefined ? new Date() : // Not provided
      (timestamp instanceof Date) ? new Date(timestamp) : // JS Date()
      new Date(timestamp * 1000) // UNIX timestamp (auto-convert to int)
    );
    return format.replace(formatChr, formatChrCb);
  };
  return this.date(format, timestamp);
}

// 将任何英文文本的日期时间描述解析为 Unix 时间戳 
function strtotime(text, now) {
  //  discuss at: http://phpjs.org/functions/strtotime/
  //     version: 1109.2016
  // original by: Caio Ariede (http://caioariede.com)
  // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // improved by: Caio Ariede (http://caioariede.com)
  // improved by: A. Matías Quezada (http://amatiasq.com)
  // improved by: preuter
  // improved by: Brett Zamir (http://brett-zamir.me)
  // improved by: Mirko Faber
  //    input by: David
  // bugfixed by: Wagner B. Soares
  // bugfixed by: Artur Tchernychev
  //        note: Examples all have a fixed timestamp to prevent tests to fail because of variable time(zones)
  //   example 1: strtotime('+1 day', 1129633200);
  //   returns 1: 1129719600
  //   example 2: strtotime('+1 week 2 days 4 hours 2 seconds', 1129633200);
  //   returns 2: 1130425202
  //   example 3: strtotime('last month', 1129633200);
  //   returns 3: 1127041200
  //   example 4: strtotime('2009-05-04 08:30:00 GMT');
  //   returns 4: 1241425800

  var parsed, match, today, year, date, days, ranges, len, times, regex, i, fail = false;

  if (!text) {
    return fail;
  }

  // Unecessary spaces
  text = text.replace(/^\s+|\s+$/g, '')
    .replace(/\s{2,}/g, ' ')
    .replace(/[\t\r\n]/g, '')
    .toLowerCase();

  // in contrast to php, js Date.parse function interprets:
  // dates given as yyyy-mm-dd as in timezone: UTC,
  // dates with "." or "-" as MDY instead of DMY
  // dates with two-digit years differently
  // etc...etc...
  // ...therefore we manually parse lots of common date formats
  match = text.match(
    /^(\d{1,4})([\-\.\/\:])(\d{1,2})([\-\.\/\:])(\d{1,4})(?:\s(\d{1,2}):(\d{2})?:?(\d{2})?)?(?:\s([A-Z]+)?)?$/);

  if (match && match[2] === match[4]) {
    if (match[1] > 1901) {
      switch (match[2]) {
        case '-':
          { // YYYY-M-D
            if (match[3] > 12 || match[5] > 31) {
              return fail;
            }

            return new Date(match[1], parseInt(match[3], 10) - 1, match[5],
              match[6] || 0, match[7] || 0, match[8] || 0, match[9] || 0) / 1000;
          }
        case '.':
          { // YYYY.M.D is not parsed by strtotime()
            return fail;
          }
        case '/':
          { // YYYY/M/D
            if (match[3] > 12 || match[5] > 31) {
              return fail;
            }

            return new Date(match[1], parseInt(match[3], 10) - 1, match[5],
              match[6] || 0, match[7] || 0, match[8] || 0, match[9] || 0) / 1000;
          }
      }
    } else if (match[5] > 1901) {
      switch (match[2]) {
        case '-':
          { // D-M-YYYY
            if (match[3] > 12 || match[1] > 31) {
              return fail;
            }

            return new Date(match[5], parseInt(match[3], 10) - 1, match[1],
              match[6] || 0, match[7] || 0, match[8] || 0, match[9] || 0) / 1000;
          }
        case '.':
          { // D.M.YYYY
            if (match[3] > 12 || match[1] > 31) {
              return fail;
            }

            return new Date(match[5], parseInt(match[3], 10) - 1, match[1],
              match[6] || 0, match[7] || 0, match[8] || 0, match[9] || 0) / 1000;
          }
        case '/':
          { // M/D/YYYY
            if (match[1] > 12 || match[3] > 31) {
              return fail;
            }

            return new Date(match[5], parseInt(match[1], 10) - 1, match[3],
              match[6] || 0, match[7] || 0, match[8] || 0, match[9] || 0) / 1000;
          }
      }
    } else {
      switch (match[2]) {
        case '-':
          { // YY-M-D
            if (match[3] > 12 || match[5] > 31 || (match[1] < 70 && match[1] > 38)) {
              return fail;
            }

            year = match[1] >= 0 && match[1] <= 38 ? +match[1] + 2000 : match[1];
            return new Date(year, parseInt(match[3], 10) - 1, match[5],
              match[6] || 0, match[7] || 0, match[8] || 0, match[9] || 0) / 1000;
          }
        case '.':
          { // D.M.YY or H.MM.SS
            if (match[5] >= 70) { // D.M.YY
              if (match[3] > 12 || match[1] > 31) {
                return fail;
              }

              return new Date(match[5], parseInt(match[3], 10) - 1, match[1],
                match[6] || 0, match[7] || 0, match[8] || 0, match[9] || 0) / 1000;
            }
            if (match[5] < 60 && !match[6]) { // H.MM.SS
              if (match[1] > 23 || match[3] > 59) {
                return fail;
              }

              today = new Date();
              return new Date(today.getFullYear(), today.getMonth(), today.getDate(),
                match[1] || 0, match[3] || 0, match[5] || 0, match[9] || 0) / 1000;
            }

            return fail; // invalid format, cannot be parsed
          }
        case '/':
          { // M/D/YY
            if (match[1] > 12 || match[3] > 31 || (match[5] < 70 && match[5] > 38)) {
              return fail;
            }

            year = match[5] >= 0 && match[5] <= 38 ? +match[5] + 2000 : match[5];
            return new Date(year, parseInt(match[1], 10) - 1, match[3],
              match[6] || 0, match[7] || 0, match[8] || 0, match[9] || 0) / 1000;
          }
        case ':':
          { // HH:MM:SS
            if (match[1] > 23 || match[3] > 59 || match[5] > 59) {
              return fail;
            }

            today = new Date();
            return new Date(today.getFullYear(), today.getMonth(), today.getDate(),
              match[1] || 0, match[3] || 0, match[5] || 0) / 1000;
          }
      }
    }
  }

  // other formats and "now" should be parsed by Date.parse()
  if (text === 'now') {
    return now === null || isNaN(now) ? new Date()
      .getTime() / 1000 | 0 : now | 0;
  }
  if (!isNaN(parsed = Date.parse(text))) {
    return parsed / 1000 | 0;
  }

  date = now ? new Date(now * 1000) : new Date();
  days = {
    'sun': 0,
    'mon': 1,
    'tue': 2,
    'wed': 3,
    'thu': 4,
    'fri': 5,
    'sat': 6
  };
  ranges = {
    'yea': 'FullYear',
    'mon': 'Month',
    'day': 'Date',
    'hou': 'Hours',
    'min': 'Minutes',
    'sec': 'Seconds'
  };

  function lastNext(type, range, modifier) {
    var diff, day = days[range];

    if (typeof day !== 'undefined') {
      diff = day - date.getDay();

      if (diff === 0) {
        diff = 7 * modifier;
      } else if (diff > 0 && type === 'last') {
        diff -= 7;
      } else if (diff < 0 && type === 'next') {
        diff += 7;
      }

      date.setDate(date.getDate() + diff);
    }
  }

  function process(val) {
    var splt = val.split(' '), // Todo: Reconcile this with regex using \s, taking into account browser issues with split and regexes
      type = splt[0],
      range = splt[1].substring(0, 3),
      typeIsNumber = /\d+/.test(type),
      ago = splt[2] === 'ago',
      num = (type === 'last' ? -1 : 1) * (ago ? -1 : 1);

    if (typeIsNumber) {
      num *= parseInt(type, 10);
    }

    if (ranges.hasOwnProperty(range) && !splt[1].match(/^mon(day|\.)?$/i)) {
      return date['set' + ranges[range]](date['get' + ranges[range]]() + num);
    }

    if (range === 'wee') {
      return date.setDate(date.getDate() + (num * 7));
    }

    if (type === 'next' || type === 'last') {
      lastNext(type, range, num);
    } else if (!typeIsNumber) {
      return false;
    }

    return true;
  }

  times = '(years?|months?|weeks?|days?|hours?|minutes?|min|seconds?|sec' +
    '|sunday|sun\\.?|monday|mon\\.?|tuesday|tue\\.?|wednesday|wed\\.?' +
    '|thursday|thu\\.?|friday|fri\\.?|saturday|sat\\.?)';
  regex = '([+-]?\\d+\\s' + times + '|' + '(last|next)\\s' + times + ')(\\sago)?';

  match = text.match(new RegExp(regex, 'gi'));
  if (!match) {
    return fail;
  }

  for (i = 0, len = match.length; i < len; i++) {
    if (!process(match[i])) {
      return fail;
    }
  }

  // ECMAScript 5 only
  // if (!match.every(process))
  //    return false;

  return (date.getTime() / 1000);
}

// 把预定义的字符 "<" （小于）和 ">" （大于）转换为 HTML 实体
function htmlspecialchars(string, quote_style, charset, double_encode) {
  //       discuss at: http://phpjs.org/functions/htmlspecialchars/
  //      original by: Mirek Slugen
  //      improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  //      bugfixed by: Nathan
  //      bugfixed by: Arno
  //      bugfixed by: Brett Zamir (http://brett-zamir.me)
  //      bugfixed by: Brett Zamir (http://brett-zamir.me)
  //       revised by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  //         input by: Ratheous
  //         input by: Mailfaker (http://www.weedem.fr/)
  //         input by: felix
  // reimplemented by: Brett Zamir (http://brett-zamir.me)
  //             note: charset argument not supported
  //        example 1: htmlspecialchars("<a href='test'>Test</a>", 'ENT_QUOTES');
  //        returns 1: '&lt;a href=&#039;test&#039;&gt;Test&lt;/a&gt;'
  //        example 2: htmlspecialchars("ab\"c'd", ['ENT_NOQUOTES', 'ENT_QUOTES']);
  //        returns 2: 'ab"c&#039;d'
  //        example 3: htmlspecialchars('my "&entity;" is still here', null, null, false);
  //        returns 3: 'my &quot;&entity;&quot; is still here'

  var optTemp = 0,
    i = 0,
    noquotes = false;
  if (typeof quote_style === 'undefined' || quote_style === null) {
    quote_style = 2;
  }
  string = string.toString();
  if (double_encode !== false) {
    // Put this first to avoid double-encoding
    string = string.replace(/&/g, '&amp;');
  }
  string = string.replace(/</g, '&lt;')
    .replace(/>/g, '&gt;');

  var OPTS = {
    'ENT_NOQUOTES'          : 0,
    'ENT_HTML_QUOTE_SINGLE' : 1,
    'ENT_HTML_QUOTE_DOUBLE' : 2,
    'ENT_COMPAT'            : 2,
    'ENT_QUOTES'            : 3,
    'ENT_IGNORE'            : 4
  };
  if (quote_style === 0) {
    noquotes = true;
  }
  if (typeof quote_style !== 'number') {
    // Allow for a single string or an array of string flags
    quote_style = [].concat(quote_style);
    for (i = 0; i < quote_style.length; i++) {
      // Resolve string input to bitwise e.g. 'ENT_IGNORE' becomes 4
      if (OPTS[quote_style[i]] === 0) {
        noquotes = true;
      } else if (OPTS[quote_style[i]]) {
        optTemp = optTemp | OPTS[quote_style[i]];
      }
    }
    quote_style = optTemp;
  }
  if (quote_style & OPTS.ENT_HTML_QUOTE_SINGLE) {
    string = string.replace(/'/g, '&#039;');
  }
  if (!noquotes) {
    string = string.replace(/"/g, '&quot;');
  }

  return string;
}